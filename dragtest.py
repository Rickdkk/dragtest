import sys
import os

import numpy as np
from PySide2 import QtWidgets
from PySide2.QtGui import QIcon
import qdarkstyle as qdarkstyle

from mainwindow_dragtest import Ui_MainWindow


class ApplicationWindow(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(ApplicationWindow, self).__init__()
        self.force_labels = []  # labels for force values from dragtest.txt
        self.actual_angle_boxes = []  # comboboxes with real angle values
        self.default_angle_labels = []  # labels for angles from dragtest.txt
        self.data_forces, self.data_angles, self.data_actual_angles = [], [], []
        self.filename = ""

        icon = self.resource_path("Accessibility_icon_small2.png")

        self.setWindowIcon(QIcon(icon))
        self.setupUi(self)  # Inherited from Ui_MainWindow
        self.initialize_connections()  # Connect all buttons
        self.make_iterables()  # Initialize lists for labels
        self.statusBar().showMessage("Application Ready, please import a drag test file")

    def initialize_connections(self):
        """Connect all buttons to their respective functions"""
        self.pb_Import.clicked.connect(self.import_drag_test)
        self.pb_Calc_Force.clicked.connect(self.calc_specific_force)
        self.sb_Target_Force.valueChanged.connect(self.calc_pulley_weight)
        self.pb_Regression.clicked.connect(self.plot_regression)
        self.pb_Export.clicked.connect(self.export_drag_test)

    def make_iterables(self):
        """Make iterables for labels so we can loop through them"""
        self.force_labels = [self.label_Force0, self.label_Force1, self.label_Force2, self.label_Force3,
                             self.label_Force4, self.label_Force5, self.label_Force6, self.label_Force7,
                             self.label_Force8, self.label_Force9]  # shhh
        self.default_angle_labels = [self.label_Angle0, self.label_Angle1, self.label_Angle2, self.label_Angle3,
                                     self.label_Angle4, self.label_Angle5, self.label_Angle6, self.label_Angle7,
                                     self.label_Angle8, self.label_Angle9]
        self.actual_angle_boxes = [self.sb_Actual0, self.sb_Actual1, self.sb_Actual2, self.sb_Actual3,
                                   self.sb_Actual4, self.sb_Actual5, self.sb_Actual6, self.sb_Actual7,
                                   self.sb_Actual8, self.sb_Actual9]

    def import_drag_test(self):
        (self.filename, _) = QtWidgets.QFileDialog.getOpenFileName(self,
                                                                   "Open Drag Test file", filter="Drag Test (*.txt)")
        if not self.filename:
            self.statusBar().showMessage(f"No file selected!")
            return  # escape if there is nothing to read

        self.data_angles, self.data_forces, self.data_actual_angles = [], [], []  # reset if user loads second file

        with open(self.filename, "r") as f:
            [next(f) for _ in range(33)]  # Skip header and crap
            for line in f:
                data = line.split()
                self.data_angles.append(float(data[0]))
                self.data_forces.append(float(data[1]))
        self.data_forces = np.array(self.data_forces)
        self.data_angles = np.array(self.data_angles)
        self.fill_labels()
        self.plot_scatter()

        self.statusBar().showMessage("Currently opened: " + self.filename)
        self.pb_Calc_Force.setEnabled(False)
        self.pb_Export.setEnabled(False)
        self.pb_Regression.setEnabled(True)
        self.sb_Target_Force.setEnabled(False)

    def fill_labels(self):
        for force_label, force in zip(self.force_labels, self.data_forces):
            force_label.setText(f"{force:.2f}")
        for angle_label, angle in zip(self.default_angle_labels, self.data_angles):
            angle_label.setText(f"{angle:.2f}")

    def get_user_input(self):
        self.data_actual_angles = []  # reset
        for box in self.actual_angle_boxes:
            self.data_actual_angles.append(box.value())
        self.data_actual_angles = np.array(self.data_actual_angles)

    def calc_specific_force(self):
        m_default, c_default, m_user, c_user = self.linear_regression()
        angle = self.sb_Input_Angle.value()
        if self.cb_User_Input.isChecked():
            force = angle * m_user + c_user
            self.sb_CalcForce.setValue(force)
        else:
            force = angle * m_default + c_default
            self.sb_CalcForce.setValue(force)
        self.plotWidget.canvas.ax.plot([angle], [force], 'ro')
        self.plotWidget.canvas.draw()
        self.sb_Target_Force.setEnabled(True)
        self.statusBar().showMessage(f"Calculated force for angle: {angle}, "
                                     f"based on user input: {self.cb_User_Input.isChecked()}!")

    def calc_pulley_weight(self):
        ZAK, SET, BOUT, MOER, RING, g = 0.411, 0.101, 0.060, 0.015, 0.0055, 9.81  # constants for pulley
        target_force = self.sb_Target_Force.value()
        drag_force = self.sb_CalcForce.value()
        remaining = (target_force - drag_force) / g  # in kg

        if remaining >= 0:
            self.label_Loc.setText("Behind")
        else:
            self.label_Loc.setText("Front")
        remaining = abs(remaining)

        if remaining >= ZAK:
            remaining -= ZAK
            self.label_BagYN.setText("Yes")
        else:
            self.label_BagYN.setText("No")
        self.label_Sets.setText(f"{remaining / SET:.0f}")
        remaining %= SET
        self.label_Bolts.setText(f"{remaining // BOUT:.0f}")
        remaining %= BOUT
        self.label_Nuts.setText(f"{remaining // MOER:.0f}")
        remaining %= MOER
        self.label_Rings.setText(f"{remaining // RING:.0f}")
        remaining %= RING
        self.pb_Export.setEnabled(True)
        self.statusBar().showMessage(f"Calculated pulley contents for target force: "
                                     f"{target_force:.2f}N, with drag force: {drag_force:.2f}N!")

    def plot_scatter(self):
        self.get_user_input()
        self.plotWidget.canvas.reset()
        axis = self.plotWidget.canvas.ax
        if np.any(self.data_angles):
            axis.plot(self.data_angles, self.data_forces, 'o', label="Default")
        if np.any(self.data_actual_angles):
            axis.plot(self.data_actual_angles, self.data_forces, 'o', label="User input")
        axis.autoscale(tight=True)
        axis.legend(loc=1, frameon=True)
        self.plotWidget.canvas.draw()

    def plot_regression(self):
        self.plot_scatter()
        m_default, c_default, m_user, c_user = self.linear_regression()
        axis = self.plotWidget.canvas.ax
        angles_range = np.linspace(0, 3.5)
        if np.any(self.data_angles):
            axis.plot(angles_range, m_default*angles_range + c_default, label="Default regression")
        if np.any(self.data_actual_angles):
            axis.plot(angles_range, m_user*angles_range + c_user, label="User input regression")
        axis.legend(loc=1, frameon=True)
        self.plotWidget.canvas.draw()

    def linear_regression(self):
        """Fit a line y = mx + c through data"""
        self.get_user_input()
        a = np.vstack([self.data_angles, np.ones(len(self.data_angles))]).T
        m_default, c_default = np.linalg.lstsq(a, self.data_forces, rcond=None)[0]
        a = np.vstack([self.data_actual_angles, np.ones(len(self.data_actual_angles))]).T
        m_user, c_user = np.linalg.lstsq(a, self.data_forces, rcond=None)[0]
        self.pb_Calc_Force.setEnabled(True)
        return m_default, c_default, m_user, c_user

    # noinspection PyTupleAssignmentBalance
    def export_drag_test(self):
        exportname = self.filename.split(".txt")[0] + "_DT.txt"
        with open(exportname, "w") as f:
            f.write(f"Drag test information for {self.filename}\n")
            f.write(f"User angle\tDefault angle\tForce\n")
            for user_angle, default_angle, force in zip(self.data_actual_angles, self.data_angles, self.data_forces):
                f.write(f"{user_angle:.2f}\t{default_angle:.2f}\t{force:.2f}\n")
            f.write(f"\nCalculated force (from regression): {self.sb_CalcForce.value()}"
                    f"\nTarget force (from coast-down): {self.sb_Target_Force.value()}"
                    f"\nAngle used (during propulsion testing): {self.sb_Input_Angle.value()}"
                    f"\nBased on user input angles: {self.cb_User_Input.isChecked()}")
            f.write(f"\nBag: {self.label_BagYN.text()}, Location: {self.label_Loc.text()}, "
                    f"Sets: {self.label_Sets.text()}, Bolts: {self.label_Bolts.text()}, "
                    f"Nuts: {self.label_Nuts.text()}, Rings: {self.label_Rings.text()}\n")
        self.statusBar().showMessage(f"Exported to {exportname}!")

    @staticmethod
    def resource_path(relative_path):
        """ Get absolute path to resource, works for dev and for PyInstaller """
        try:
            # PyInstaller creates a temp folder and stores path in _MEIPASS
            base_path = sys._MEIPASS
        except AttributeError:
            base_path = os.path.abspath(".")
        return os.path.join(base_path, relative_path)


def main():
    app = QtWidgets.QApplication(sys.argv)
    app.setStyleSheet(qdarkstyle.load_stylesheet())
    application = ApplicationWindow()
    application.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
