from PySide2 import QtWidgets
import matplotlib
matplotlib.use('QT5Agg')  # Ensure using PyQt5 backend

from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as Canvas
import matplotlib.pyplot as plt
plt.style.use("dark_background")


# Matplotlib canvas class to create figure
class MplCanvas(Canvas):
    def __init__(self):
        self.fig = Figure()
        self.ax = self.fig.add_subplot(111)
        self.reset()
        Canvas.__init__(self, self.fig)
        Canvas.setSizePolicy(self, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        Canvas.updateGeometry(self)

    def reset(self):
        self.ax.clear()
        self.ax.set_xlim(0, 10)
        self.ax.set_title("Regression for drag test")
        self.ax.set_xlabel("Treadmill angle (degrees)")
        self.ax.set_ylabel("Load cell force (Newtons)")
        self.fig.set_tight_layout(True)


# Matplotlib widget
class MplWidget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)   # Inherit from QWidget
        self.canvas = MplCanvas()                  # Create canvas object
        self.vbl = QtWidgets.QVBoxLayout()         # Set box for plotting
        self.vbl.addWidget(self.canvas)
        self.setLayout(self.vbl)